public class SLIP {
    private final int SLIP_END = 0300;
    private final int SLIP_ESC = 0333;
    private final int SLIP_ESC_END = 0334;
    private final int SLIP_ESC_ESC = 0335;

    public int framingIndex(int[] byteList)
    {
        int count = 0;
        for(int i: byteList)
        {
            if(i == SLIP_END || i == SLIP_ESC)
            {
                count ++;
            }
        }
        count += byteList.length + 2;
        return count;
    }
    public int deframingIndex(int[] byteList)
    {
        int count = -2; //앞 뒤 SLIP_END값 제거
        for(int i = 0; i < byteList.length; i++)
        {
            if(byteList[i] == SLIP_ESC && byteList[i+1] == SLIP_ESC_END)
                continue;
            else if(byteList[i] == SLIP_ESC && byteList[i+1] == SLIP_ESC_ESC)
                continue;
            else
                count++;
        }
        return count;
    }
    public int[] framing(int[] byteList)
    {
        int count = 1;
        int[] dataBuffer = new int[framingIndex(byteList)];
        dataBuffer[0] = SLIP_END;
        for(int i: byteList)
        {
            if(i == SLIP_END)
            {
                dataBuffer[count] = SLIP_ESC;
                dataBuffer[count+1] = SLIP_ESC_END;
                count+=2;
            }
            else if(i == SLIP_ESC)
            {
                dataBuffer[count] = SLIP_ESC;
                dataBuffer[count+1] = SLIP_ESC_ESC;
                count+=2;
            }
            else
            {
                dataBuffer[count] = i;
                count++;
            }
        }
        dataBuffer[count] = SLIP_END;
        return dataBuffer;
    }
    public int[] deFraming(int[] byteList)
    {
        int[] dataBuffer = new int[deframingIndex(byteList)];
        for(int i = 0, count = 0; i < byteList.length; i++)
        {
            if(i == 0) {
                continue;
            }
            else if(byteList[i] == SLIP_END)
            {
                return dataBuffer;
            }
            else if(byteList[i] == SLIP_ESC)
            {
                if(byteList[i+1] == SLIP_ESC_END)
                {
                    dataBuffer[count] = SLIP_END; i++; count++;
                }
                else if(byteList[i+1] == SLIP_ESC_ESC)
                {
                    dataBuffer[count] = SLIP_ESC; i++; count++;
                }
            }
            else
            {
                dataBuffer[count] = byteList[i];
                count++;
            }
        }
        return dataBuffer;
    }
}
